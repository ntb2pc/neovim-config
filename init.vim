set laststatus=2
set encoding=utf-8
syntax on
set scrolloff=15
set showcmd
set showmatch
set hidden
set noerrorbells

set autochdir
set tabstop=2
set shiftwidth=2
set expandtab

set relativenumber
set number
set noshowmode

let mapleader = ","

nnoremap <F8> :TagbarToggle<CR>
nnoremap <leader>f :Autoformat<CR>
nnoremap <leader>nt :NERDTreeToggle<CR>
nnoremap <leader>h :noh<CR>

map <c-h> :bp<CR>
map <c-l> :bn<CR>

set background=dark
let g:hybrid_custom_term_colors = 1
let g:hybrid_reduced_contrast = 1
colorscheme hybrid

let g:airline_theme = 'papercolor'
let g:airline_powerline_fonts = 1
let g:airline#extensions#branch#enabled = 1
let g:airline_exclude_preview = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#fnamemod = ':t'
let g:airline#extensions#tabline#buffer_nr_show = 1

let g:UltiSnipsExpandTrigger="<c-j>"
let g:UltiSnipsJumpForwardTrigger="<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-k>"

let g:ycm_rust_src_path = '/home/nick/.rustsrc/rustc-1.7.0/src'
let g:ycm_path_to_python_interpreter = '/usr/bin/python'
let g:ycm_auto_trigger = 1
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_extra_conf_globlist = ['/home/nick/dev/*', '/home/nick/school/*']

let g:esearch = {
  \ 'adapter': 'pt',
  \ 'backend': 'nvim',
  \ 'out': 'win',
  \ 'batch_size' : 1000,
  \ 'use': ['visual', 'hlsearch', 'last'],
  \}

let g:formatterpath = ['/home/nick/misc/llvm-build/bin']
let g:autoformat_autoindent = 0

let g:jsx_ext_required = 0
autocmd FileType jsx UltiSnipsAddFileTypes html

call plug#begin('~/.config/nvim/plugged')

Plug 'sjl/gundo.vim'

Plug 'Valloric/YouCompleteMe'

Plug 'eugen0329/vim-esearch'

Plug 'critiqjo/lldb.nvim'

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

Plug 'flazz/vim-colorschemes'

Plug 'terryma/vim-multiple-cursors'

Plug 'tpope/vim-fugitive'

Plug 'scrooloose/nerdtree'

Plug 'majutsushi/tagbar'

Plug 'ap/vim-css-color'

Plug 'Chiel92/vim-autoformat'

Plug 'ervandew/supertab'

Plug 'SirVer/ultisnips'
Plug 'justinj/vim-react-snippets'
Plug 'honza/vim-snippets'

Plug 'rust-lang/rust.vim'

Plug 'dpwright/vim-tup'

Plug 'benekastah/neomake'

Plug 'pangloss/vim-javascript'
Plug 'mxw/vim-jsx'

call plug#end()
